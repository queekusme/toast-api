FROM python:3.10-slim

ENV PYTHONBUFFERED=1

WORKDIR /app
ADD requirements.txt .
RUN pip install -r requirements.txt
RUN pip install django[argon2]
ADD . .

CMD ["/bin/sh", "-c", "python manage.py collectstatic --noinput && gunicorn -w 4 -b 0.0.0.0:80 toastsite.wsgi" ]
EXPOSE 80
