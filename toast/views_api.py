from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError

from toast.models import Event, Team, League, Person, Availability, Role, Position, Game, \
    Association
from toast.serialisers import EventSerialiser, LeagueSerialiser, \
    AvailabilitySerialiser, RoleSerialiser, PositionSerialiser, GameSerialiser, AssociationSerialiser, TeamSerialiser


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    def get_queryset(self):
        #me = self.request.user.person

        start_date = self.request.query_params.get('start', None)
        end_date = self.request.query_params.get('end', None)

        qs = Event.season()

        if end_date:
            qs = qs.filter(date__lte=end_date)
        if start_date:
            qs = qs.filter(date__gte=start_date)

        return qs

    serializer_class = EventSerialiser


class LeagueViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = League.objects.all()
    serializer_class = LeagueSerialiser


class TeamViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerialiser


class RoleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Role.objects.all()
    serializer_class = RoleSerialiser


class PositionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Position.objects.all()
    serializer_class = PositionSerialiser


class GameViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerialiser


class AssociationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Association.objects.all()
    serializer_class = AssociationSerialiser


class MyAvailabilitiesViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        me: Person = Person.objects.get(user=self.request.user)
        qs = Availability.objects.filter(person=me).order_by('event__date')
        return qs

    def perform_create(self, serializer):
        me: Person = Person.objects.get(user=self.request.user)
        # Fail if I'm already busy on this day
        existing_avails = me.availability_set.filter(event__date=serializer.validated_data['event'].date)
        if existing_avails.exists():
            raise ValidationError('You may not be available for two events on the same day')
        serializer.save(person=me)

    serializer_class = AvailabilitySerialiser
