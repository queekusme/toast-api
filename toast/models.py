from typing import Callable

from django.core import mail
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.db import models
from django.conf import settings
from django.db.models import Q, QuerySet
from django.http import HttpRequest, HttpResponse, HttpResponseBadRequest
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from datetime import time
from django.db.models.signals import post_save
from django.dispatch import receiver


class Association(models.Model):  # WTFDA/MRDA/JRDA/Other
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class GameType(models.Model):  # Sanc/National/Reg/Other
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Role(models.Model):  # SO/NSO
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Position(models.Model):
    class Meta:
        ordering = ["role", "name"]
    role = models.ForeignKey('Role', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    explanatory_text = models.CharField(max_length=200, blank=True)

    def __str__(self):
        # TODO: this is somewhat verbose because the widget for the multi-select uses it in the availability form
        # would be better to do this sort of logic in the template but means writing a custom template for
        # multi-selects. We probably want to do this anyway so we can use checkboxes without JS tomfoolery
        if self.explanatory_text:
            return f'{self.name} ({self.explanatory_text})'
        return self.name


class Person(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=200)
    # recycle e-mail address, admin field from user model
    affiliation = models.CharField(max_length=200, blank=True)
    facebook_profile = models.CharField(max_length=200)
    wftda_cert_claimed_identity = models.CharField(max_length=100, blank=True)
    pronouns = models.CharField(max_length=50, blank=True)
    games_history_link = models.URLField(blank=True)
    notes = models.CharField(max_length=200, blank=True)  # Medical info, etc
    opt_in_to_update_emails = models.BooleanField(default=False)
    is_tournament_head_official = models.BooleanField(default=False)

    preferred_roles = models.ManyToManyField('Role', blank=True)
    preferred_positions = models.ManyToManyField('Position', blank=True)

    def can_create_event(self) -> bool:
        # I need to be a division admin or host league rep to create:
        i_am_league_rep = self.representative_of_leagues.exists()
        i_am_division_admin = self.admin_of_divisions.exists()
        i_am_tournament_head = self.is_tournament_head_official
        return any((i_am_division_admin, i_am_league_rep, i_am_tournament_head))

    def __str__(self):
        return self.display_name

    @property
    def availability_set(self) -> QuerySet:
        return self.availabilities.filter(rejected=False)


class TournamentTier(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class TournamentDivision(models.Model):
    name = models.CharField(max_length=100)
    tier = models.ForeignKey('TournamentTier', on_delete=models.CASCADE, related_name='tier')
    admins = models.ManyToManyField('Person', related_name='admin_of_divisions')

    def __str__(self):
        return f"{self.tier}-{self.name}"


class League(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    based_in_city = models.CharField(max_length=100)
    representatives = models.ManyToManyField('Person', blank=True, related_name='representative_of_leagues')

    def can_edit(self, user) -> bool:
        return any((user.is_staff, user.is_superuser, self.representatives.filter(pk=user.person.pk).exists()))

    def __str__(self):
        return f'{self.name}'


class Team(models.Model):
    class Meta:
        ordering = ("league__name",)
    name = models.CharField(max_length=100, db_index=True)
    league = models.ForeignKey('League', on_delete=models.CASCADE)
    tournament_division = models.ForeignKey('TournamentDivision', null=True, blank=True, on_delete=models.SET_NULL, related_name='teams')

    def games_count(self):
        n_away = Game.objects.filter(home_team=self).count()
        n_home = Game.objects.filter(away_team=self).count()
        return n_away + n_home

    def future_games_count(self):
        today = timezone.now()
        n_away = Game.objects.filter(home_team=self).filter(event__date__gt=today).count()
        n_home = Game.objects.filter(away_team=self).filter(event__date__gt=today).count()
        return n_away + n_home

    def __str__(self):
        if not self.tournament_division:
            return f'{self.league} - {self.name}'
        return f'{self.league} - {self.name} ({self.tournament_division})'


def midday():
    return time(hour=12, minute=00)


class Event(models.Model):
    class Meta:
        ordering = ['date']

    name = models.CharField(max_length=100, blank=True)
    is_tournament_game_day = models.BooleanField(default=False)  # specifically 'the tournament this site manages'
    host = models.ForeignKey('League', on_delete=models.CASCADE)
    date = models.DateField(default=timezone.now)
    start_time = models.TimeField(default=midday)
    city = models.CharField(max_length=100)
    address = models.CharField(max_length=200, blank=True)
    schedule = models.TextField(blank=True)
    last_modified = models.DateTimeField(auto_now=True)
    facebook_event_url = models.URLField(blank=True, null=True)
    officials_group_or_event_url = models.URLField(blank=True, null=True)
    crew_heads = models.ManyToManyField('Person', related_name='crew_heading_event', blank=True)
    open_roles = models.ManyToManyField('Role', related_name='open_for_event', blank=True, default=Role.objects.all)

    @property
    def friendly_name(self):
        if self.name:
            return f'{self.host.name} present: {self.name}'
        else:
            return f"{self.host.name}'s Event"

    def __str__(self):
        insert = f'present {self.name}' if self.name else 'event'
        return f'{self.host} {insert} on {self.date} in {self.city}'

    @classmethod
    def season(cls) -> QuerySet:
        # We redefine season as 'past events up to x months ago and future events up to y months from now'
        today = timezone.now()
        earliest_date = today - timezone.timedelta(weeks=settings.TOAST_SEASON_PAST_HORIZON_WEEKS)
        latest_date = today - timezone.timedelta(weeks=settings.TOAST_SEASON_FUTURE_HORIZON_WEEKS)
        return cls.objects.all().filter(date__gte=earliest_date).filter(date__gte=latest_date).order_by('date')

    @property
    def datetime(self):
        return timezone.datetime.combine(self.date, self.start_time)

    def leagues_query(self) -> QuerySet[League]:
        # We want Leagues whose Teams are playing in this Event
        return League.objects.filter(team__in=self.participants_teams_query()).order_by('pk').distinct()

    def tournament_leagues_participating_query(self) -> QuerySet[League]:
        # We want Leagues whose Teams are playing in this Event
        return League.objects.filter(team__in=self.tournament_participants_teams_query()).order_by('pk').distinct()

    def tournament_division_head_query(self) -> QuerySet[Person]:
        return Person.objects.filter(admin_of_divisions__in=self.participants_divisions_query()).order_by('pk').distinct()

    def participants_divisions_query(self) -> QuerySet[TournamentDivision]:
        return TournamentDivision.objects.filter(teams__in=self.participants_teams_query()).order_by('pk').distinct()

    # Participant Host - Team(s) of the Host League who is Playing in this Event
    def host_participating_divisions_query(self) -> QuerySet[TournamentDivision]:
        return TournamentDivision.objects.filter(teams__in=self.host_participating_teams_query()).order_by('pk').distinct()

    def participants_teams_query(self) -> QuerySet[Team]:
        # We want Teams who are playing in this Event
        return Team.objects.filter(Q(home_games__in=self.game_set.all()) | Q(away_games__in=self.game_set.all())).order_by('pk').distinct()

    def tournament_participants_teams_query(self) -> QuerySet[Team]:
        # We want Teams who are playing in this Event excluding those who aren't participating in the tourney
        return self.participants_teams_query().exclude(tournament_division=None)

    def host_divisions_query(self) -> QuerySet[TournamentDivision]:
        return TournamentDivision.objects.filter(teams__in=self.host_teams_query()).order_by('pk').distinct()

    def host_teams_query(self) -> QuerySet[Team]:
        return Team.objects.filter(league=self.host)

    def host_participating_teams_query(self) -> QuerySet[Team]:
        return self.participants_teams_query().filter(league=self.host)

    @property
    def noms_due_in_days(self):
        return (self.date - settings.TOAST_NOMINATION_DEADLINE_DAYSBEFORE - timezone.now().date()).days

    @property
    def noms_due_on(self):
        return self.date - settings.TOAST_NOMINATION_DEADLINE_DAYSBEFORE

    def interested_people_query(self) -> QuerySet[Person]:
        return Person.objects.filter(
            Q(availabilities__in=self.availability_set.all()) |
            Q(representative_of_leagues__in=self.leagues_query()) |
            Q(admin_of_divisions__in=self.participants_divisions_query()), opt_in_to_update_emails=True
        ).order_by('pk').distinct()

    def interested_people_email_addresses(self):
        return self.interested_people_query().values_list('user__email', flat=True)

    def send_changed_emails(self, request):
        # e-mail anyone who marked themselves as available for this, League Reps and RHOs.
        # IF they've opted in
        if not settings.TOAST_SEND_UPDATE_EMAILS:
            return
        my_url = request.build_absolute_uri(reverse('show_event', args=[self.pk]))
        root_url = request.build_absolute_uri(reverse('index'))

        people = self.interested_people_query()
        if not people.exists():
            return
        with mail.get_connection() as connection:
            for person in people:
                text = loader.render_to_string('toast/event_changed_email.txt', {'event': self, 'person': person, 'event_url': my_url, 'root_url': root_url, 'TOAST_NAME': settings.TOAST_NAME})
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} An Event you're interested in has changed",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)

    def send_delete_emails(self, request):
        root_url = request.build_absolute_uri(reverse('index'))
        people = self.interested_people_query()
        if not people:
            return
        with mail.get_connection() as connection:
            for person in people:
                text = loader.render_to_string('toast/event_deleted_email.txt', {'event': self, 'person': person, 'TOAST_NAME': settings.TOAST_NAME, 'root_url': root_url})
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} An Event you're interested in has been removed",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)

    def is_tournament_division_head(self, who: Person):
        return self.tournament_division_head_query().filter(pk=who.pk).exists()

    def can_edit_basic_data(self, who: Person):
        can_edit = self.can_edit(who)
        i_am_host = self.host.representatives.filter(pk=who.pk).exists()

        return can_edit or i_am_host

    def can_edit(self, who: Person):
        i_am_a_division_admin = self.is_tournament_division_head(who)
        i_am_a_tournament_head = who.is_tournament_head_official
        return any((i_am_a_division_admin, who.user.is_staff, who.user.is_superuser, i_am_a_tournament_head))

    def is_head_official(self, who: Person):
        i_am_a_division_admin = self.is_tournament_division_head(who)
        i_am_tournament_head = who.is_tournament_head_official
        i_am_crew_head = self.crew_heads.filter(pk=who.pk).exists()
        return any((i_am_a_division_admin, i_am_crew_head, i_am_tournament_head))

    def can_delete_event(self, person: Person) -> bool:
        # I can delete it if I'm a rep for the host league or a sysadmin,
        # or it's a tournament game day & I'm an admin for one of the host league's teams divisions
        is_sysadmin = person.user.is_staff
        is_league_rep = person.representative_of_leagues.filter(pk=self.host.pk).exists()
        is_tournament_division_admin = self.is_tournament_division_head(person)
        i_am_tournament_head = person.is_tournament_head_official

        return any((is_league_rep, is_tournament_division_admin, is_sysadmin, i_am_tournament_head))

    def am_in_charge(self, person: Person) -> bool:
        i_can_delete = self.can_delete_event(person)
        i_am_head_official = self.is_head_official(person)
        return i_can_delete or i_am_head_official

    def lock_role_for_crew(self, request: HttpRequest, role: Role, send_email: bool = False) -> HttpResponse:
        # Lock-in the event crew
        # Send e-mails to all Availabilities - success to those set marked_for_crewing, commiserations otherwise.
        # Only RHOs may do this.
        if not self.open_roles.filter(pk=role.id).exists():
            # Already locked. Don't send e-mails twice!
            return HttpResponse()

        selected_officials_for_role = self.availability_set.filter(selected_for_crew_role=role)

        if not selected_officials_for_role.exists():
            return HttpResponseBadRequest(f"You have not selected a crew for the {role.name} yet! Click the Role Rostering button(s) next to your selections!")

        self.open_roles.remove(role)
        self.save()

        if settings.TOAST_SEND_CREWING_EMAILS and send_email:
            self.email_from_request(request,
                                    lambda my_url, root_url: self.email_role_congrats(my_url, root_url, role, selected_officials_for_role))

        # ONLY SEND COMMISERATIONS AND FREE UP AVAILS WHEN ALL ROLES LOCKED
        # otherwise we reject people while we still have holes to fill
        if not self.open_roles.exists():  # no unlocked roles exist - all holes filled
            unselected_officials = self.availability_set.filter(selected_for_crew=False)

            if settings.TOAST_SEND_CREWING_EMAILS and send_email:
                self.email_from_request(request, lambda my_url, root_url: self.email_commiserations(my_url, root_url, unselected_officials))

            # Free up the availability for people who were not selected.
            unselected_officials.exclude(person__pk__in=self.crew_heads.all()).update(rejected=True)

        return HttpResponse()

    def email_from_request(self, request: HttpRequest, sender_body_lambda: Callable[[str, str], None]):
        my_url = request.build_absolute_uri(reverse('show_event', args=[self.pk]))
        root_url = request.build_absolute_uri(reverse('index'))
        sender_body_lambda(my_url, root_url)

    def email_role_congrats(self, my_url: str, root_url: str, role: Role, selected_officials: QuerySet):
        with mail.get_connection() as connection:
            ctx = {
                'event': self,
                'event_url': my_url,
                'root_url': root_url,
                'TOAST_NAME': settings.TOAST_NAME
            }
            for selected in selected_officials:
                ctx['person'] = selected.person
                ctx['role'] = role.name
                text = loader.render_to_string('toast/event_selected_email.txt', ctx)
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} You've been rostered for an event!",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [selected.person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)

    def email_commiserations(self, my_url: str, root_url: str, unselected_officials: QuerySet):
        with mail.get_connection() as connection:
            ctx = {
                'event': self,
                'event_url': my_url,
                'root_url': root_url,
                'TOAST_NAME': settings.TOAST_NAME
            }
            for unselected in unselected_officials:
                ctx['person'] = unselected.person
                text = loader.render_to_string('toast/event_not_selected_email.txt', ctx)
                email = EmailMessage(
                    f"{settings.EMAIL_SUBJECT_PREFIX} You were not rostered for an event",
                    text,
                    settings.DEFAULT_FROM_EMAIL,
                    [unselected.person.user.email],
                    connection=connection
                )
                email.send(fail_silently=True)

    @property
    def availability_set(self) -> QuerySet:
        # Dirty cheat - patch over the old name used throughout the code. Sorry.
        return self.availabilities.filter(rejected=False)

    @property
    def rejected_availabilities(self) -> QuerySet:
        return self.availabilities.filter(rejected=True)

    @property
    def crew_is_fully_locked(self) -> bool:
        # locked if no holes left
        return not self.open_roles.exists()

    def role_is_locked(self, role: Role) -> bool:
        return not self.open_roles.filter(pk=role.pk).exists()


class Game(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    home_team = models.ForeignKey('Team', on_delete=models.CASCADE, related_name='home_games')
    away_team = models.ForeignKey('Team', on_delete=models.CASCADE, related_name='away_games')
    # TODO: this isn't the correct way to add constraints, c.f.
    # TODO: https://docs.djangoproject.com/en/3.0/ref/models/options/#django.db.models.Options.constraints
    different_teams_constraint = models.UniqueConstraint(fields=['home_team', 'away_team'], name='different_teams')
    game_type = models.ForeignKey('GameType', on_delete=models.CASCADE)
    association = models.ForeignKey('Association', on_delete=models.CASCADE)

    def home_team_nom_count(self):
        return self.nom_count(self.home_team.league)

    def away_team_nom_count(self):
        return self.nom_count(self.away_team.league)

    def nom_count(self, league):
        return league.availability_set.filter(event__game=self).distinct().count()

    def teams_query(self) -> QuerySet:
        return Team.objects.filter(Q(home_games=self) | Q(away_games=self)).distinct()

    def tournament_division(self):
        # Both teams should be in the same tier!
        # (iff this is a Tournament game)
        return self.home_team.tournament_division


class Availability(models.Model):
    class Meta:
        verbose_name_plural = "Availabilities"

    person = models.ForeignKey('Person', on_delete=models.CASCADE, related_name='availabilities')
    event = models.ForeignKey('Event', on_delete=models.CASCADE, related_name='availabilities')
    notes = models.CharField(max_length=200, blank=True)
    roles = models.ManyToManyField('Role', verbose_name='Role (please choose at least one)')
    preferences = models.ManyToManyField('Position', blank=True, verbose_name='Position Preferences (optional)')
    selected_for_crew = models.BooleanField(default=False)
    selected_for_crew_role = models.ForeignKey('Role', blank=True, null=True, on_delete=models.SET_NULL, related_name='selected_officials')
    rejected = models.BooleanField(default=False)  # Rejected Availabilities don't count or show up anywhere
    nominating_league = models.ForeignKey('League', blank=True, null=True, on_delete=models.SET_NULL,
                                        verbose_name='Nominating League (please leave blank unless specifically asked by a League)')

    def validate_unique(self, *args, **kwargs):
        super().validate_unique(*args, **kwargs)
        qs = self.person.availability_set.exclude(rejected=True).filter(event__date=self.event.date).exclude(pk=self.pk)
        if qs.exists():
            raise ValidationError('You may not be available for more than one event one the same day')

    def __str__(self):
        return f"{self.person.display_name}'s availability for {self.event}"


class Settings(models.Model):
    class Meta:
        verbose_name_plural = "Settings"

    # A singleton model.
    def save(self, *args, **kwargs):
        self.pk = 1
        super(Settings, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_person_object(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return
    if raw:
        return
    user = instance
    p = Person.objects.create(user=user, display_name=user.first_name or 'No Name')

