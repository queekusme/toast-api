import datetime

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q, QuerySet
from django.forms import inlineformset_factory
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import get_user_model, login, logout
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, JsonResponse, \
    HttpResponseServerError, HttpRequest, HttpResponseForbidden, HttpResponseNotFound
from django.urls import reverse
from django.utils import timezone
from django.contrib import messages

from toast.forms import CreateAccountForm, AvailabilityForm, PersonForm, EventForm, InlineGamesFormSet, CreateEventForm, \
    CreateLeagueForm, InlineTeamsFormSet, NominatingLeagueForm, InviteNewLeagueRepForm
from toast.models import Person, Event, League, Availability, Game, Role, Position, Team, TournamentDivision

INCOMPLETE_PROFILE_MESSAGE = 'It looks like you have not added a Facebook Profile or Best Contact Method. ' \
    'We will need this in order to add you to Crews. Please fill it in and save your Profile.'


def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home'))

    return render(request, 'toast/landing.html')


def create_account(request):
    ctx = {}

    if request.method == 'POST':
        usermodel = get_user_model()
        f = CreateAccountForm(request.POST)
        if f.is_valid():
            email = f.cleaned_data['email']
            email = email.lower()
            name = f.cleaned_data['derbyname']
            pass1 = f.cleaned_data['password1']
            pass2 = f.cleaned_data['password2']

            if usermodel.objects.filter(username=email).exists():
                f.add_error('email', 'This e-mail address is in use. Did you forget your password?')

            elif pass1 != pass2:
                f.add_error('password1', 'Passwords did not match!')
            else:
                # create user!
                user = usermodel.objects.create_user(
                    email,
                    email,
                    pass1,
                    first_name=name
                )
                login(request, user)

                return HttpResponseRedirect(reverse('myprofile'))

    else:
        f = CreateAccountForm()

    ctx['create_form'] = f
    return render(request, 'toast/create_account.html', ctx)


@login_required
def current_season(request: HttpRequest):
    now = timezone.now()
    from_date = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)

    me: Person = Person.objects.get(user=request.user)

    if not me.facebook_profile:
        messages.add_message(request, messages.INFO, INCOMPLETE_PROFILE_MESSAGE)
        return HttpResponseRedirect(reverse('myprofile'))

    events = Event.season()
    if from_date:
        events = events.filter(date__gte=from_date)
    my_availabilities = me.availability_set.filter(event__in=events)
    # transform list to dict -- is this faster than just doing one DB query for every event?
    my_events_to_availabilities = {
        avail.event: avail for avail in my_availabilities
    }
    event_to_availability = {
        event: my_events_to_availabilities.get(event) for event in events
    }
    my_administered_divisions = me.admin_of_divisions.all()
    my_rep_teams = me.representative_of_leagues.all()
    ctx = {
        'me': me,
        'events': events,
        'event_to_avail': event_to_availability,
        'my_administered_divisions': my_administered_divisions,
        'my_rep_teams': my_rep_teams,

    }
    return render(request, 'toast/season.html', ctx)


@login_required
def home(request):
    me: Person = Person.objects.get(user=request.user)

    if not me.facebook_profile:
        messages.add_message(request, messages.INFO, INCOMPLETE_PROFILE_MESSAGE)
        return HttpResponseRedirect(reverse('myprofile'))

    return render(request, 'toast/home.html')


@login_required
def edit_availability(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)

    if event.crew_is_fully_locked:
        return HttpResponseBadRequest('The crew for this event is locked.')

    me: Person = Person.objects.get(user=request.user)
    ctx = {
        'me': me,
        'event': event,
    }

    # Get the leagues involved in the day. Needed for nominations.
    leagues_involved = event.tournament_leagues_participating_query()

    # Is there already an availability?
    avail = me.availability_set.filter(event=event)

    if request.method == 'POST':
        if avail.exists():
            f = AvailabilityForm(request.POST, instance=avail.first())
            f.fields['nominating_league'].queryset = leagues_involved
            f.fields['roles'].queryset = event.open_roles.all()
            f.fields['preferences'].queryset = Position.objects.filter(role__in=event.open_roles.all())
        else:
            f = AvailabilityForm(request.POST)
            f.fields['nominating_league'].queryset = leagues_involved
            f.fields['roles'].queryset = event.open_roles.all()
            f.fields['preferences'].queryset = Position.objects.filter(role__in=event.open_roles.all())

        if f.is_valid():
            avail = f.save()

            # Fixup: anyone who'se chosen a position preference but not the corresponding role should have the role
            # auto-selected
            roles = Role.objects.filter(position__in=avail.preferences.all()).distinct()
            for role in roles:
                avail.roles.add(role)

            return HttpResponseRedirect(reverse('show_event', args=[eventid]))

    else:
        if avail.exists():
            # Yes. Show a bound form.
            f = AvailabilityForm(instance=avail.first())
            f.fields['nominating_league'].queryset = leagues_involved
            f.fields['roles'].queryset = event.open_roles.all()
            f.fields['preferences'].queryset = Position.objects.filter(role__in=event.open_roles.all())
        else:
            # No - this is a new availability for this date.
            # ENSURE I'M NOT OVERCOMMITTING.
            existing_avails = me.availability_set.filter(event__date=event.date)
            if existing_avails.exists():
                return render(request, 'toast/already_committed.html', {'event': existing_avails.first().event})

            # Show an empty form.
            # Auto-configure avail with default user preferences & roles
            f = AvailabilityForm(initial={
                "person": me,
                "event": event,
                "roles": me.preferred_roles.all(),
                "preferences": me.preferred_positions.all()
            })
            f.fields['nominating_league'].queryset = leagues_involved
            f.fields['roles'].queryset = event.open_roles.all()
            f.fields['preferences'].queryset = Position.objects.filter(role__in=event.open_roles.all())

    ctx['form'] = f

    return render(request, 'toast/edit_availability.html', ctx)


@login_required
def del_availability(request, availabilityid):
    a = get_object_or_404(Availability, pk=availabilityid)
    me: Person = Person.objects.get(user=request.user)
    if a.person_id != me.pk:
        return HttpResponseBadRequest('Nice try, this is not your availability')

    # You may not pull out of something you've been rostered for
    if a.selected_for_crew_role:
        if a.event.role_is_locked(a.selected_for_crew_role):
            return HttpResponseBadRequest(f'The {a.selected_for_crew_role} crew for this event is locked.')

    a.delete()

    return HttpResponseRedirect(reverse('show_event', args=[a.event.pk]))


@login_required
def edit_availability_nomination(request, availabilityid):
    avail: Availability = get_object_or_404(Availability, pk=availabilityid)
    event: Event = avail.event
    me: Person = Person.objects.get(user=request.user)
    them: Person = avail.person

    leagues_involved = event.leagues_query()

    if not event.can_edit_basic_data(me):
        return HttpResponse('YOOOOU SHALL NOOOOT edit this event')

    if not event.is_tournament_game_day:
        return HttpResponse('This is not a tournament game day - no nominations required!')

    if request.method == 'POST':
        form = NominatingLeagueForm(request.POST, instance=avail)
        form.fields['nominating_league'].queryset = leagues_involved

        if form.is_valid():
            avail = form.save()
            return HttpResponseRedirect(reverse('show_event', args=[event.pk]))

    else:
        form = NominatingLeagueForm(instance=avail)
        form.fields["nominating_league"].queryset = leagues_involved

    ctx = {
        'form': form,
        'avail': avail,
        'event': event,
        'who': them
    }
    return render(request, 'toast/edit_nominating_league.html', ctx)


@login_required
def myprofile(request):
    me: Person = Person.objects.get(user=request.user)
    ctx = {
        'me': me
    }
    if request.method == 'POST':
        f = PersonForm(request.POST, instance=me)
        if f.is_valid():
            me = f.save()
            request.user.first_name = me.display_name
            request.user.email = f["email"].value()
            request.user.user_name = request.user.email
            request.user.save()

            return HttpResponseRedirect(reverse('index'))
    else:
        f = PersonForm(instance=me, initial={'email': me.user.email})

    ctx['form'] = f

    return render(request, 'toast/account_setup.html', ctx)


@login_required
def show_event(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)

    if not me.facebook_profile:
        messages.add_message(request, messages.INFO, INCOMPLETE_PROFILE_MESSAGE)
        return HttpResponseRedirect(reverse('myprofile'))

    # Only the relevant head official can see things. Or staff.
    i_am_crew_head_official = event.crew_heads.filter(pk=me.pk).exists()
    i_am_an_admin = event.can_delete_event(me)
    i_am_in_charge_of_the_day = i_am_an_admin or i_am_crew_head_official

    my_avail = event.availability_set.filter(person=me)
    if my_avail.exists():
        my_avail = my_avail.first()
    else:
        my_avail = None

    # League reps can see their own nominees.
    my_teams_nominees = event.availability_set.filter(nominating_league__in=me.representative_of_leagues.all()).distinct()

    event_title = f'{event.host.name} present: {event.name}' if event.name else f'{event.host.name} host in {event.city} on {event.date}'

    ctx = {
        'event': event,
        'event_title': event_title,
        'me': me,
        'i_am_in_charge': i_am_in_charge_of_the_day,
        'i_am_an_admin': i_am_an_admin,
        'my_avail': my_avail,
        'my_teams_nominees': my_teams_nominees,
        'roles': Role.objects.all(),
        'locked_roles': Role.objects.exclude(pk__in=event.open_roles.all()),
        'all_avails': event.availability_set.all().order_by('person__display_name'),
        'rejected_avails': event.rejected_availabilities.all().order_by('person__display_name'),
        'tournament_heads': Person.objects.filter(is_tournament_head_official=True)
    }

    return render(request, 'toast/show_event.html', ctx)


@login_required
def export_event_to_csv(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)

    if not event.am_in_charge(me):
        return HttpResponse('Nice try. You are not a head official for this event.')

    response = HttpResponse(content_type='text/plain')

    headers = [
        'display_name',
        'affiliation',
        'fb_or_contact',
        'pronouns',
        'wftda_id',
        'history',
        'note',
    ]

    if event.is_tournament_game_day:
        headers.append('nominating_league')

    headers.extend(Role.objects.all().values_list('name', flat=True))
    headers.extend(Position.objects.all().values_list('name', flat=True))
    response.write("; ".join(headers))
    response.write("\r\n")

    for avail in event.availability_set.all().select_related('person').order_by('person'):
        response.write(f"{avail.person.display_name}; ")
        response.write(f"{avail.person.affiliation}; ")
        response.write(f"{avail.person.facebook_profile}; ")
        response.write(f"{avail.person.pronouns}; ")
        response.write(f"{avail.person.wftda_cert_claimed_identity}; ")
        response.write(f"{avail.person.games_history_link}; ")
        response.write(f"{avail.person.notes.replace(',', ' ')}; ")
        if event.is_tournament_game_day:
            response.write(f"{avail.nominating_league}; ")
        # Oh god I'm so sorry please somehow re-write this
        for r in Role.objects.all():
            response.write("Y; " if r.availability_set.filter(pk=avail.pk).exists() else "; ")
        for p in Position.objects.all():
            response.write("Y; " if p.availability_set.filter(pk=avail.pk).exists() else "; ")
        response.write("\r\n")

    return response


@login_required
def create_league(request: HttpRequest) -> HttpResponse:
    me: Person = Person.objects.get(user=request.user)

    if request.method == 'POST':
        form = CreateLeagueForm(request.POST)
        teams_form = InlineTeamsFormSet(request.POST, request.FILES)
        ctx = {
            'form': form,
            'teams_form': teams_form
        }
        if form.is_valid():
            if teams_form.is_valid():
                league = form.save()
                me.representative_of_leagues.add(league)

                teams_form.instance = league
                teams_form.save()
                return HttpResponseRedirect(reverse('current_season'))
    else:
        form = CreateLeagueForm()
        teams_form = InlineTeamsFormSet

        ctx = {
            'form': form,
            'teams_form': teams_form
        }
    return render(request, 'toast/create_league.html', ctx)


@login_required
def create_event(request: HttpRequest) -> HttpResponse:
    me: Person = Person.objects.get(user=request.user)
    if not me.facebook_profile:
        messages.add_message(request, messages.INFO, INCOMPLETE_PROFILE_MESSAGE)
        return HttpResponseRedirect(reverse('myprofile'))

    can_create_event = me.can_create_event()
    if not can_create_event:
        return HttpResponse("You must be a registered Representative of a League or an Administrator of a Tier to create events.")

    # If I'm a league rep I can only use one of my leagues as the host
    # If I'm a division admin I can create an event for any league containing a team in my divisions + my leagues
    # if I'm a tournament head I can see all leagues participating in tourney + my league
    my_leagues: QuerySet = me.representative_of_leagues.all()
    leagues_i_oversee_in_tournament = League.objects.filter(team__tournament_division__in=me.admin_of_divisions.all())
    all_tournament_leagues = League.objects.filter(team__tournament_division__isnull=False)
    possible_leagues = my_leagues | leagues_i_oversee_in_tournament
    if me.is_tournament_head_official:
        possible_leagues = possible_leagues | all_tournament_leagues
    possible_leagues = possible_leagues.order_by('pk').distinct().order_by('name')

    if request.method == 'POST':
        form = CreateEventForm(request.POST)
        form.fields['host'].queryset = possible_leagues
        games_form = InlineGamesFormSet(request.POST, request.FILES)
        ctx = {
            'form': form,
            'games_form': games_form
        }
        if form.is_valid():
            if games_form.is_valid():
                new_event: Event = form.save()
                # TODO: fix this in the ctor or whatever
                new_event.open_roles.add(*list(Role.objects.all()))
                new_event.save()
                games_form.instance = new_event
                games_form.save()
                return HttpResponseRedirect(reverse('show_event', args=[new_event.pk]))
    else:
        form = CreateEventForm()
        form.fields['host'].queryset = possible_leagues
        games_form = InlineGamesFormSet()
        ctx = {
            'form': form,
            'games_form': games_form
        }
    return render(request, 'toast/create_event.html', ctx)


@login_required
def del_event(request: HttpRequest, eventid: int) -> HttpResponse:
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    i_can_delete = event.can_delete_event(me)
    if not i_can_delete:
        return HttpResponseForbidden('You may not delete this event')

    event.delete()
    return HttpResponseRedirect(reverse('current_season'))


@login_required
def edit_event(request, eventid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)

    # I need to be admin, staff, an RHO for this event or a team rep for the host
    if not event.can_edit_basic_data(me):
        return HttpResponse('YOOOOU SHALL NOOOOT edit this event')

    ctx = {
        'event': event,
        'me': me
    }
    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        games_form = InlineGamesFormSet(request.POST, request.FILES, instance=event)
        if form.is_valid():
            me = form.save()
            if games_form.is_valid():
                games_form.instance = me
                games_form.save()
            return HttpResponseRedirect(reverse('show_event', args=[event.pk]))
    else:
        form = EventForm(instance=event)
        games_form = InlineGamesFormSet(instance=event)

    ctx['form'] = form
    ctx['games_form'] = games_form

    return render(request, 'toast/edit_event.html', ctx)


@login_required
def your_events(request: HttpRequest):
    now = timezone.now()
    from_date = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)

    me: Person = Person.objects.get(user=request.user)

    if not me.facebook_profile:
        messages.add_message(request, messages.INFO, INCOMPLETE_PROFILE_MESSAGE)
        return HttpResponseRedirect(reverse('myprofile'))

    # Show me Tournament events I'm responsible as Tournament Division Admin or crew head!
    my_divisions = me.admin_of_divisions.all()
    my_tournament_events_as_division_admin_or_crew_head = Event.objects.all().filter(is_tournament_game_day=True).filter(
        Q(game__away_team__tournament_division__in=my_divisions) |
        Q(game__home_team__tournament_division__in=my_divisions) |
        Q(crew_heads=me)
    )
    if me.is_tournament_head_official:
        my_tournament_events_as_division_admin_or_crew_head = my_tournament_events_as_division_admin_or_crew_head | Event.objects.filter(is_tournament_game_day=True)
    my_tournament_events_as_division_admin_or_crew_head = my_tournament_events_as_division_admin_or_crew_head.order_by('date').distinct()

    # Show me non-tournament games I've been deleted as Crew Head
    my_non_tournament_events_as_crew_head = Event.objects.all().filter(is_tournament_game_day=False).filter(crew_heads=me)

    # Show me non-Tournament events I'm responsible for as League Rep
    my_leagues = me.representative_of_leagues.all()
    my_non_tournament_events_as_league_rep = Event.objects.filter(host__in=my_leagues)
    #my_non_tournament_events_as_league_rep = Game.objects.filter(away_team__league__in=my_leagues) | Game.objects.filter(home_team__league__in=my_leagues)
    my_non_tournament_events_as_league_rep = my_non_tournament_events_as_league_rep.order_by('date')

    # Show me Tournament games I'm responsible for as Team Rep - where is my team playing
    my_leagues = me.representative_of_leagues.all()
    my_tournament_games_as_league_rep = Game.objects\
        .filter(event__is_tournament_game_day=True)\
        .filter(Q(away_team__league__in=my_leagues) | Q(home_team__league__in=my_leagues))
    my_tournament_games_as_league_rep = my_tournament_games_as_league_rep.order_by('event__date')

    if from_date:
        my_tournament_events_as_division_admin_or_crew_head = my_tournament_events_as_division_admin_or_crew_head.filter(date__gte=from_date)
        my_non_tournament_events_as_league_rep = my_non_tournament_events_as_league_rep.filter(date__gte=from_date)
        my_non_tournament_events_as_crew_head = my_non_tournament_events_as_crew_head.filter(date__gte=from_date)
        my_tournament_games_as_league_rep = my_tournament_games_as_league_rep.filter(event__date__gte=from_date)

    my_tournament_games_as_league_rep_game_to_noms_count = {
        game: game.event.availability_set.filter(nominating_league__in=my_leagues).distinct().count()
        for game in my_tournament_games_as_league_rep
    }

    ctx = {
        'me': me,
        'my_tournament_events_as_division_admin_or_crew_head': my_tournament_events_as_division_admin_or_crew_head,
        'my_non_tournament_events_as_league_rep': my_non_tournament_events_as_league_rep,
        'my_non_tournament_events_as_crew_head': my_non_tournament_events_as_crew_head,
        'my_tournament_games_as_league_rep': my_tournament_games_as_league_rep,
        'my_tournament_games_as_league_rep_game_to_noms_count': my_tournament_games_as_league_rep_game_to_noms_count
    }
    return render(request, 'toast/my_events.html', ctx)


@login_required
def your_avails(request: HttpRequest):
    now = timezone.now()
    from_date = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)
    me: Person = Person.objects.get(user=request.user)

    if not me.facebook_profile:
        messages.add_message(request, messages.INFO, INCOMPLETE_PROFILE_MESSAGE)
        return HttpResponseRedirect(reverse('myprofile'))

    # Show me availabilities I've made this season
    avails = me.availability_set.order_by('event__date')

    if from_date:
        avails = avails.filter(event__date__gte=from_date)

    ctx = {
        'me': me,
        'avails': avails
    }
    return render(request, 'toast/my_avails.html', ctx)


def transform_event_to_calendardata(instance: Event):
    # See https://fullcalendar.io/docs/event-parsing for spec
    return {
            'id': instance.pk,
            'url': reverse('show_event', args=[instance.pk]),
            'title': f'{instance.host.name} host at {instance.city}',
            'start': instance.date.isoformat(),
            'allDay': True,
        }


@login_required
def current_season_cal(request):
    events = Event.season()
    payload = [
        transform_event_to_calendardata(event)
        for event in events

    ]
    return JsonResponse(payload, safe=False)


@login_required
def delete_account(request):
    return render(request, 'toast/delete_account.html')


@login_required
def really_delete_account(request):
    user = request.user
    logout(request)
    success = user.delete()
    if not success:
        return HttpResponseServerError("Couldn't delete for some reason")

    return HttpResponseRedirect(reverse('index'))


def format_ical_date_string(dt):
    # c.f. https://www.kanzaki.com/docs/ical/dateTime.html
    return dt.strftime('%Y%m%dT%H%M%SZ')


def gen_ical(request, event_query: QuerySet) -> HttpResponse:
    event_duration = timezone.timedelta(hours=settings.TOAST_EVENT_DURATION_HOURS)
    resp = HttpResponse(content_type='text/calendar')

    resp.write('BEGIN:VCALENDAR\r\n')
    resp.write('VERSION:2.0\r\n')
    resp.write('PRODID:-//RDEC//NONSGML v1.0//EN\r\n')
    now = timezone.now()

    then = now - datetime.timedelta(days=settings.TOAST_CALENDAR_GOBACK_DAYS)

    event_list = event_query.filter(date__gt=then).order_by('date')
    for e in event_list:
        end = e.datetime + event_duration

        url = reverse('show_event', args=[e.pk])
        url = request.build_absolute_uri(url)

        summary = f'{e.host.name} host in {e.city}'
        description = summary
        if e.name:
            summary = f'{e.name}'
        description = f'{description}\\n\r\n {url}'

        resp.write('BEGIN:VEVENT\n')
        resp.write(f'UID:{e.pk}@{request.get_host()}\r\n')
        resp.write(f'DTSTAMP:{format_ical_date_string(e.last_modified)}\r\n')
        resp.write(f'DTSTART:{format_ical_date_string(e.datetime)}\r\n')
        resp.write(f'DTEND:{format_ical_date_string(end)}\r\n')
        resp.write(f'SUMMARY:{summary}\r\n')
        resp.write(f'DESCRIPTION:{description}\r\n')
        resp.write(f'LOCATION:{e.city}\r\n')
        resp.write(f'URL:{url}\r\n')

        resp.write('END:VEVENT\r\n')

    resp.write('END:VCALENDAR\r\n')
    return resp


def ical(request):
    return gen_ical(request, Event.objects.all())


def tournament_ical(request):
    return gen_ical(request, Event.objects.filter(is_tournament_game_day=True))


@login_required
def league_rep_report(request: HttpRequest):
    me: Person = Person.objects.get(user=request.user)

    if not any((me.user.is_staff, me.user.is_superuser, me.is_tournament_head_official)):
        return HttpResponseBadRequest('Nice try.')

    leagues_with_reps = League.objects.all().order_by('name')
    return render(request, 'toast/team_rep_report.html', {'leagues': leagues_with_reps})


@login_required
def make_crew_head(request, eventid, personid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    new_head: Person = get_object_or_404(Person, pk=personid)

    # I need to be a Division Admin or League Rep
    if not event.can_delete_event(me):
        return HttpResponseBadRequest('Nice try. You are not an RHO for this event.')

    # The selected Crew Head needs to be rostered!
    # Because otherwise construction the e-mail list is just too hard.
    availability: Availability = event.availability_set.get(person__pk=new_head.pk)
    if not availability.selected_for_crew:
        return HttpResponseBadRequest("You must first Roster this person before making them a Crew Head by clicking one of the Role buttons!")

    event.crew_heads.add(new_head)

    return HttpResponse()


@login_required
def remove_crew_head(request, eventid, personid):
    event: Event = get_object_or_404(Event, pk=eventid)
    me: Person = Person.objects.get(user=request.user)
    old_head: Person = get_object_or_404(Person, pk=personid)

    # I need to be a Division Admin or League Rep
    if not event.can_delete_event(me):
        return HttpResponseBadRequest('Nice try. You are not an RHO for this event.')

    event.crew_heads.remove(old_head)

    return HttpResponse()


@login_required
def lock_in_crew_role_and_email(request, event_id, role_id):
    event: Event = get_object_or_404(Event, pk=event_id)
    role: Role = get_object_or_404(Role, pk=role_id)
    me: Person = Person.objects.get(user=request.user)
    if not event.am_in_charge(me):
        return HttpResponseBadRequest("You must be a DHO to perform this action")

    return event.lock_role_for_crew(request, role, True)


@login_required
def lock_in_crew_role(request, event_id, role_id):
    event: Event = get_object_or_404(Event, pk=event_id)
    role: Role = get_object_or_404(Role, pk=role_id)
    me: Person = Person.objects.get(user=request.user)
    if not event.am_in_charge(me):
        return HttpResponseBadRequest("You must be a DHO to perform this action")

    return event.lock_role_for_crew(request, role)


@login_required
def select_official(request, availabilityid, roleid):
    availability = get_object_or_404(Availability, pk=availabilityid)
    role = get_object_or_404(Role, pk=roleid)
    event: Event = availability.event
    me: Person = Person.objects.get(user=request.user)

    i_am_in_charge = event.am_in_charge(me)
    if not i_am_in_charge:
        return HttpResponseBadRequest("You must be a League Rep, Division Head or CHO to perform this action")

    # if I was already rostered for a locked role - stop
    if availability.selected_for_crew_role:
        if event.role_is_locked(availability.selected_for_crew_role):
            return HttpResponseBadRequest(f"Cannot unroster - the event's {availability.selected_for_crew_role} crew is locked in.")
    # if I am trying to be added to a locked role - stop
    if event.role_is_locked(role):
        # Too late.
        return HttpResponseBadRequest(f"The event's {role} crew is already locked in.")

    availability.selected_for_crew = True
    availability.selected_for_crew_role = role

    availability.save()
    return HttpResponse()


@login_required
def unselect_official(request, availabilityid):
    availability = get_object_or_404(Availability, pk=availabilityid)
    event: Event = availability.event
    me: Person = Person.objects.get(user=request.user)
    i_am_in_charge = event.am_in_charge(me)
    if not i_am_in_charge:
        return HttpResponseBadRequest("You must be a League Rep, Division Admin or CHO to perform this action")

    if availability.selected_for_crew_role:
        if event.role_is_locked(availability.selected_for_crew_role):
            # Too late.
            return HttpResponseBadRequest(f"The event's {availability.selected_for_crew_role } crew is already locked in.")

    availability.selected_for_crew = False
    availability.selected_for_crew_role = None
    event.crew_heads.remove(availability.person)

    availability.save()

    return HttpResponse()


@login_required()
def edit_league(request, leagueid: int):
    InlineTeamsFormEditSet = inlineformset_factory(
        League,
        Team,
        min_num=1,
        fields=('name', 'tournament_division'),
        extra=2
    )
    league: League = get_object_or_404(League, pk=leagueid)
    # I must be a rep of this league, or admin
    if not league.can_edit(request.user):
        return HttpResponseForbidden("You are not an admin or league rep for this league.")

    ctx = {
        "league": league
    }

    if request.method == 'POST':

        new_rep = None
        if 'editform_submit' not in request.POST:
            # No edit form data provided - construct an empty one
            league_edit_form = CreateLeagueForm(instance=league)
            teams_edit_form = InlineTeamsFormEditSet(instance=league)
        else:
            # Edit form data provided - use data from form
            league_edit_form = CreateLeagueForm(request.POST, instance=league)
            teams_edit_form = InlineTeamsFormEditSet(request.POST, request.FILES, instance=league)

            if league_edit_form.is_valid():
                league_edit_form.save()

                if teams_edit_form.is_valid():
                    teams_edit_form.save()  # DANGER this will silently delete any games (and noms!)

                # Reload so location.reload() will do a GET if we call it later...
                return HttpResponseRedirect(reverse('league', args=(league.pk,)))

        if 'invite_rep_form_submit' not in request.POST:
            invite_new_rep_form = InviteNewLeagueRepForm()
        else:
            invite_new_rep_form = InviteNewLeagueRepForm(request.POST)
            if invite_new_rep_form.is_valid():
                # if e-mail address provided, use that first
                search_for_mail = invite_new_rep_form.cleaned_data['email']
                search_for_name = invite_new_rep_form.cleaned_data['derby_name']
                if search_for_mail:
                    try:
                        new_rep = get_user_model().objects.get(email=search_for_mail)
                        new_rep = new_rep.person
                    except get_user_model().DoesNotExist:
                        # else try to look up with display name
                        invite_new_rep_form.add_error('email', "Could not find a user with this e-mail address.")

                if search_for_name:
                    try:
                        new_rep = Person.objects.get(display_name__iexact=search_for_name)
                    except Person.DoesNotExist:
                        invite_new_rep_form.add_error('derby_name', "Could not find a user with this display name.")
                    except Person.MultipleObjectsReturned:
                        invite_new_rep_form.add_error('derby_name', "Found multiple people with the same display name - please ask your victim for their e-mail address and use that instead.")

                if new_rep is not None:
                    # ADD NEW REP!
                    new_rep.representative_of_leagues.add(league)

                    # Reload so location.reload() will do a GET if we call it later...
                    return HttpResponseRedirect(reverse('league', args=(league.pk,)))
    else:
        invite_new_rep_form = InviteNewLeagueRepForm()
        league_edit_form = CreateLeagueForm(instance=league)
        teams_edit_form = InlineTeamsFormEditSet(instance=league)

    ctx["invite_new_rep_form"] = invite_new_rep_form
    ctx["league_edit_form"] = league_edit_form
    ctx["teams_edit_form"] = teams_edit_form

    return render(request, 'toast/edit_league.html', ctx)


@login_required()
def sack_rep(request, league_id: int, person_id: int):
    league: League = get_object_or_404(League, pk=league_id)

    # I must be a rep of this league, or admin
    if not league.can_edit(request.user):
        return HttpResponseForbidden("You are not an admin or league rep for this league.")

    # Person must be a rep already
    try:
        person: Person = league.representatives.get(pk=person_id)
    except Person.DoesNotExist:
        return HttpResponseNotFound("This person is not a League Rep for this league.")

    # Cannot delete last rep
    if league.representatives.count() < 2:
        return HttpResponseBadRequest("You cannot delete the last representative of the league. Add a successor and try again!")

    # Remove the rep!
    league.representatives.remove(person)

    return HttpResponse()


@login_required()
def delete_team(request, league_id: int, team_id: int):
    league: League = get_object_or_404(League, pk=league_id)

    # I must be a rep of this league, or admin
    if not league.can_edit(request.user):
        return HttpResponseForbidden("You are not an admin or league rep for this league.")

    try:
        team: Team = league.team_set.get(pk=team_id)
    except Team.DoesNotExist:
        return HttpResponseNotFound("Team not found")

    # Cannot delete last team
    if league.team_set.count() < 2:
        return HttpResponseBadRequest("You cannot delete the last team in league. Add more teams and try again!")

    # Delete the team!
    num_deleted, type_2_deletion_count_dict = team.delete()
    payload = dict({"games_deleted": type_2_deletion_count_dict['toast.Game'] if 'toast.Game' in type_2_deletion_count_dict else 0})  # since num = team (1)+ games (N)
    return JsonResponse(payload)


@login_required()
def admin_spring_clean(request):
    me = request.user.person
    if not any((me.user.is_staff, me.user.is_superuser)):
        return HttpResponseBadRequest('Nice try.')

    now = timezone.now()
    horizon = timezone.timedelta(weeks=settings.TOAST_SPRING_CLEAN_HORIZON_WEEKS)
    then = now - horizon

    scheduled_for_termination = Event.objects.filter(date__lt=then)

    if request.method == 'POST':
        # TODO: confirmation. In fact just re-write this whole bit
        scheduled_for_termination.delete()
        return HttpResponseRedirect(reverse('spring_clean'))

    ctx = {
        'horizon': then,
        'events': scheduled_for_termination
    }
    return render(request, 'toast/admin_spring_clean.html', ctx)
