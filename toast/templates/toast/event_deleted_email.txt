Hello {{person.display_name}},

You're receiving this because you're interested in the Event hosted by {{event.host | safe}} on {{event.date}} in {{event.city}}.

Please note that this event has just been removed.

Why not take the day off?

Many thanks,
{{ TOAST_NAME }} Tournament Officials Season Tracker
{{ root_url }}


You're getting this because you're an Official who was available for the event, a League Rep or a Division HO.
You can opt out of these e-mails by editing your Profile - click on your name in the TOaST site.
