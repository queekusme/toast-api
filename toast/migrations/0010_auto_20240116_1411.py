# Generated by Django 3.2 on 2024-01-16 14:11

from django.db import migrations, models
import django.db.models.manager


def setup_locks(apps, schema_editor):
    Event = apps.get_model("toast", "Event")
    Role = apps.get_model("toast", "Role")
    roles = list(Role.objects.all())
    for e in Event.objects.exclude(crew_locked=True):
        e.open_roles.add(*roles)
        e.save()


class Migration(migrations.Migration):

    dependencies = [
        ('toast', '0009_auto_20240116_1246'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='open_roles',
            field=models.ManyToManyField(blank=True, default=django.db.models.manager.BaseManager.all, related_name='open_for_event', to='toast.Role'),
        ),
        migrations.RunPython(setup_locks),
        migrations.RemoveField(
            model_name='event',
            name='crew_locked',
        )
    ]
