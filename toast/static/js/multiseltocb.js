var multiSelects = document.querySelectorAll('select[multiple]');

for (var i = 0; i < multiSelects.length; i++) {
    var select = multiSelects[i];
    var parent = select.parentElement;
    options = select.querySelectorAll('option');

    for (var j = 0; j < options.length; j++) {
        var checked = options[j].getAttribute('selected') !== null ? true : false;

        var formGroup = document.createElement('div');
        formGroup.classList.add('form-check');

        var checkbox = document.createElement('input');
        checkbox.setAttribute('type','checkbox');
        checkbox.setAttribute('id', 'checkBox_'+select.getAttribute('name')+"_"+options[j].value)
        checkbox.classList.add('form-check-input');
        checkbox.setAttribute('name', options[j].value);
        checkbox.setAttribute('data-target', select.getAttribute('name'));
        if (checked) checkbox.setAttribute('checked', true);
        checkbox.addEventListener('change', checkboxChanged);

        var label = document.createElement('label');
        label.setAttribute('for', 'checkBox_'+select.getAttribute('name')+"_"+options[j].value)
        label.classList.add('form-check-label');
        label.innerHTML = options[j].innerHTML;

        formGroup.appendChild(checkbox);
        formGroup.appendChild(label);
        parent.append(formGroup);
    }

    select.classList.add('d-none');
}

function checkboxChanged(e) {
    var value = e.target.getAttribute('name');
    var target = e.target.getAttribute('data-target');
    var checked = e.target.checked;
    var select = document.querySelector('select[multiple][name="' + target + '"]');
    var option = select.querySelector('option[value="' + value + '"]');
    checked ? option.setAttribute('selected', true) : option.removeAttribute('selected');
}
