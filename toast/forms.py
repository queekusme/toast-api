from django import forms
from django.core.exceptions import ValidationError
from django.forms import inlineformset_factory
from django.conf import settings

from toast.models import Availability, Person, Event, Game, League, Team


class CreateAccountForm(forms.Form):
    email = forms.EmailField(label='Your e-mail address')
    derbyname = forms.CharField(label='Your public/Derby name')
    password1 = forms.CharField(label='Your Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm Your Password', widget=forms.PasswordInput)


class AvailabilityForm(forms.ModelForm):
    class Meta:
        model = Availability
        fields = [
            'person',
            'event',
            'notes',
            'nominating_league',
            'roles',
            'preferences'
        ]


class NominatingLeagueForm(forms.ModelForm):
    class Meta:
        model = Availability
        fields = [
            'nominating_league',
        ]


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = [
            'display_name',
            'email',
            'affiliation',
            'facebook_profile',
            'wftda_cert_claimed_identity',
            'pronouns',
            'games_history_link',
            'opt_in_to_update_emails',
            'notes',
            'preferred_roles',
            'preferred_positions',
        ]
    email = forms.EmailField(label='Email address')


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'name',
            'date',
            'is_tournament_game_day',
            'start_time',
            'city',
            'address',
            'schedule',
            'facebook_event_url',
            'officials_group_or_event_url',
        ]
        labels = {
            'name': 'Event Name/Short Description (optional)',
            'is_tournament_game_day': f'Is {settings.TOAST_TOURNAMENT_NAME} Tournament Game Day?'
        }


class CreateEventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'name',
            'host',
            'date',
            'is_tournament_game_day',
            'start_time',
            'city',
            'address',
            'schedule',
            'facebook_event_url',
            'officials_group_or_event_url',
        ]
        labels = {
            'name': 'Event Name/Short Description (optional)',
            'is_tournament_game_day': f'Is {settings.TOAST_TOURNAMENT_NAME} Tournament Game Day?'
        }


InlineGamesFormSet = inlineformset_factory(
    Event,
    Game,
    fields=('home_team', 'away_team', 'game_type', 'association'),
    min_num=1,
    extra=3
)


class CreateLeagueForm(forms.ModelForm):
    class Meta:
        model = League
        fields = [
            'name',
            'based_in_city',
        ]

    def clean_name(self):
        data = self.cleaned_data["name"]
        if self.instance:
            if self.instance.name == data:
                return data
        if League.objects.filter(name__iexact=data).exists():
            raise ValidationError("This League already exists.")
        return data


InlineTeamsFormSet = inlineformset_factory(
    League,
    Team,
    min_num=1,
    fields=('name', 'tournament_division'),
    extra=5
)


class InviteNewLeagueRepForm(forms.Form):
    derby_name = forms.CharField(label='New Rep Display/Derby Name', required=False)
    email = forms.EmailField(label='New Rep E-mail Address', required=False)

    def clean(self):
        check = [self.cleaned_data['derby_name'], self.cleaned_data['email'] if 'email' in self.cleaned_data else ""]
        if any(check) and not all(check):
            # possible add some errors
            return self.cleaned_data
        raise ValidationError('Please supply either Display/Derby Name or E-mail Address (but not both!)')

