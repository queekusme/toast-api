# coding=utf-8
from django.core.management import BaseCommand
from django.db import transaction

from toast.models import Association, GameType, Role, Position


class Command(BaseCommand):
    @transaction.atomic()
    def handle(self, *args, **kwargs):

        assocs = [
            Association.objects.create(name=assoc_name)
            for assoc_name in ("WFTDA", "MRDA", "JRDA", "Other")
        ]

        game_types = [
            GameType.objects.create(name=type_name)
            for type_name in ("Scrim", "Sanctioned", "National", "Regulation", "Other")
        ]

        ref = Role.objects.create(name="Referee")
        nso = Role.objects.create(name="NSO")

        ref_roles = [
            Position.objects.create(role=ref, name=ref_role[0], explanatory_text=ref_role[1] )
            for ref_role in (
                ("HR/RIPR", "Game-Day Crew Head Referee"),
                ("FIPR", "Front Inside Pack Referee"),
                ("JR", "Jammer Referee"),
                ("OPR", "Outside Pack Referee"),
                ("AltR", "Alternate Referee"),
            )
        ]

        nso_roles = [
            Position.objects.create(role=nso, name=nso_role[0], explanatory_text=nso_role[1])
            for nso_role in (
                ("HNSO", "Game-Day Crew Head NSO"),
                ("PT", "Penalty Tracker"),
                ("JT", "Jam Timer"),
                ("PW", "Penalty Wrangler"),
                ("IWB", "Inside Whiteboard"),
                ("PBT", "Penalty Box Timer"),
                ("PBM", "Penalty Box Manager"),
                ("SK", "Scorekeeper"),
                ("SBO", "Scoreboard Operator"),
                ("LT", "Lineup Tracker"),
                ("PLT", "Penalty/Lineup Tracker"),
                ("AltN", "Alternate NSO"),
            )
        ]
