# TOaST

The Roller Derby Tournament Officials Season Tracker backend!

## Running for Production
Docker is the preferred deployment mechanism. An interim, example docker-compose file is available in the [toast-deploy](https://gitlab.com/leegent/toast-deploy) project.

## Running Locally for Development
You require Python 3.7. PostgreSQL 9.6 is optional (the Sqlite fallback is perfectly acceptable).

### Python 3.7
The use of a virtualenv or [Conda](https://docs.conda.io/en/latest/miniconda.htm) environment is strongly recommended.
Note you don't need Miniconda3 to create Python3 virtual environments.
#### Using a Miniconda environment
1. `conda create -n toast python=3.7`
1. `conda activate toast`

### Optional PostgreSQL via Docker
If you have a Docker engine available locally you can easily run a throwaway PG instance:
* `docker run --rm -d -p5432:5432 -v toastsqldata:/var/lib/postgresql/data postgres:9.6-alpine`


### Dependencies
After activating your Python environment:
1. `pip install -r requirements.txt`
2. `pip install django[argon2]` (my hand-pruned requirements.txt doesn't do this for you alas)


### Environment
1. Create file `.env`
1. Add line `DEBUG=True`
1. Add line `DATABASE_URL=sqlite:///db.sqlite3` or
1. Add line `DATABASE_URL=postgres:///pguser:pgpass@pghostname:5432/pgdbname` if you have a Postgres instance
1. Add line `DATABASE_URL=postgres://postgres:postgres@localhost:5432/postgres` if using PG via Docker as above

### One-Time Setup
1. `python manage.py migrate`
1. `python manage.py make_base_data` - this will add the basic Associations, Officiating Positions and Game Types.
1. `python manage.py make_test_data` - optional, to populate with a test Season with random teams and fixtures.
1. `python manage.py createsuperuser` - creates the superuser. Use an e-mail address as a Username.

Note that the new Admin user does not have a nice 'display name'. To set your display name, click "No Name" in the nav bar of the main site.

### Post-update setup
After pulling any new updates, it may be necessary to execute `pip install -r requirements.txt` to update the deps and
`python manage.py migrate` to update the DB schema to reflect any model/schema changes.

### Running the server
Execute `python manage.py runserver` either from a command line or from a Debugger (after activating your Python environment).

You may now use a Web browser to navigate to `http://localhost:8000`.


## Using the Application
The initial Superuser is able to use the Admin backend by clicking on the Admin link in the nav bar.

### Structure
* All Game objects (even Teams) are rooted by Season so you'll always need at least one Season object.
* Once you've created a Season, you can create Officiating Regions and Tiers for that Season.
* Once you've created a Tier and a Region, you can add Teams.
* Once you've created Teams, you can create Events.
* Once you've created Events, you can add Games to the event (it's possible to create Events from the New Game page).

### Promoting other users to Admins (Recommended only for BC staff)
* Click Users in the Admin backend. Search for the relevant user and click them.
* Check the Staff and Superuser checkboxes. Erm, I'm not actually sure which is the most necessary.
* Click the Save button

### Promoting other people to Region HO
* Click Officiating Regions in the Admin backend. Search for the relevant Region and/or filter by Season.
* Click the relevent Region.
* Select (or CTRL-select for multiple) the relevent People in the Heads table. Click Save.

### Promoting other people to Team Rep
* Click Teams in the Admin backend. Search for the relevant Team and/or filter by Season.
* Click the relevent Team.
* Select (or CTRL-select for multiple) the relevent People in the Representatives table. Click Save.


## Using the API
### Authentication
Request a token by POSTing user credentials to `/api/v1/api-token-auth`:

```curl -X POST -H 'Content-Type: application/json' -d '{"username": "<email address>", "password": "<password>"}"  https://.../api/v1/api-token-auth```

Response:
```{"token":"<token string>"}```

Add this token to all subsequent requests' Headers:

```curl -H "Authorization: Token <token string>" https://.../api/v1/my_availabilities/```

### The API
Explore the possibilities by browsing the auto-generated bobbins at `https://.../api/v1/`. Most resources are read-only
except for the Availability resources at `/api/v1/my_availabilities` - you can POST to this to mark yourself as available for an event.

The Event list accepts `start` and/or `end` dates as optional query params:
`/api/v1/events/?start=2020-02-01&end=2020-03-01`
