"""toastsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from toast.admin import admin_site
from django.urls import path, include
import django.contrib.auth.views as auth_views
from toastsite.forms import CaseInsensitiveUsernameAuthForm


urlpatterns = [
    path('', include('toast.urls')),
    path('accounts/login/', auth_views.LoginView.as_view(authentication_form=CaseInsensitiveUsernameAuthForm)),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin_site.urls),
]
