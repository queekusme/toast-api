from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm


class CaseInsensitiveUsernameAuthForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    # Lifted clean from django.contrib.auth and pasted but for the call to .lower().
    # I'm going to silicon hell
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            username = username.lower()
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data
